# My .vimrc and submodules
### to install
```
cd ~
git clone git@bitbucket.org:b0red/.vim.git ~/.vim
ln -s ~/.vim/vimrc ~/vimrc; ln -s ~/.vim/gvimrc ~/gvimrc
cd ~/.vim; git submodule init; git submodule update
```
## Installing a plugin bundle
To install a plugin and add to to the repo, add it as a submodule with; 
```
git submodule add https://github.com/flazz/vim-colorschemes bundle/colorschemes
```

## Upgrading a specific plugin bundle

At some point in the future, the fugitive plugin might be updated. To fetch the latest changes, go into the fugitive repository, and pull the latest version:
```
cd ~/.vim/bundle/fugitive
git pull origin master
```

## Upgrading all bundled plugins

You can use the foreach command to execute any shell script in from the root of all submodule directories. To update to the latest version of each plugin bundle, run the following:
```
git submodule foreach git pull origin master
```

## Removing a plugin

Assuming .vim already is in a git repo
```
$ cd ~/.vim
$ git submodule deinit -f bundle/delimitMate
$ git rm -rf bundle/delimitMate
$ # (rm -rf .git/modules/bundle/delimitMate)
```

## Installed plugins/bundles
A list of installed plugins (maybe not always up 2 date)

* colorschemes https://github.com/flazz/vim-colorschemes
* colorscheme-switcher (https://github.com/xolox/vim-colorscheme-switcher.git)
* diffconflicts https://github.com/whiteinge/diffconflicts
* lightline https://github.com/itchyny/lightline.vim
* nerdtree (https://github.com/scrooloose/nerdtree.git)
* setcolors (https://github.com/felixhummel/setcolors.vim.git)
* vim-fugutive (https://github.com/tpope/vim-fugitive.git)
* vim-misc (https://github.com/xolox/vim-misc.git)
* vim-sensible (https://github.com/tpope/vim-sensible.git)

